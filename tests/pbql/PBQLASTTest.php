<?php
// include './ModelUpdate/pbql/ast/PBQL.php';
// include './ModelUpdate/pbql/exception/ParseException.php';

// use Core\pbql\ast\PBQL;
// use Core\pbql\Exception\ParseException;

// Test pbql AST parser
class PBQLASTTest extends PHPUnit_Framework_TestCase {

    // TODO
    // - Test with different string values (dates/utf8/...)

    public function testRelationProperty () {

        $ast = new \pbql\Parser('channel.id:1');

        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Object",
                "object" => (object) [
                    "type" => "Identifier",
                    "value" => "channel"
                ],
                "property" => (object) [
                    "type" => "Identifier",
                    "value" => "id"
                ]
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => 1
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testDeepRelationProperty () {

        $ast = new \pbql\Parser('programme.channel.id:1');
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Object",
                "object" => (object) [
                    "type" => "Object",
                    "object" => (object) [
                        "type" => "Identifier",
                        "value" => "programme",
                    ],
                    "property" => (object) [
                        "type" => "Identifier",
                        "value" => "channel"
                    ]
                ],
                "property" => (object) [
                    "type" => "Identifier",
                    "value" => "id"
                ]
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => 1
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testDeeperRelationProperty () {

        $ast = new \pbql\Parser('channel.programme.item.id:1');
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Object",
                "object" => (object) [
                    "type" => "Object",
                    "object" => (object) [
                        "type" => "Object",
                        "object" => (object) [
                            "type" => "Identifier",
                            "value" => "channel"
                        ],
                        "property" => (object) [
                            "type" => "Identifier",
                            "value" => "programme"
                        ]
                    ],
                    "property" => (object) [
                        "type" => "Identifier",
                        "value" => "item"
                    ]
                ],
                "property" => (object) [
                    "type" => "Identifier",
                    "value" => "id"
                ]
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => 1
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testSimplestatementNumericValue () {

        $ast = new \pbql\Parser('id:1');
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Identifier",
                "value" => "id"
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => 1
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testSimpleStatementFloatValue () {

        $ast = new \pbql\Parser('id:3.14');
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Identifier",
                "value" => "id"
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => 3.14
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testSimpleStatementWhitespaces () {

        $ast = new \pbql\Parser('id : 1');
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Identifier",
                "value" => "id"
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => 1
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testSimplestatementStringValue () {

        $ast = new \pbql\Parser('name:"mr wong"');
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Identifier",
                "value" => "name"
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => "mr wong"
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testSimplestatementEmptyStringValue () {

        $ast = new \pbql\Parser('name:""');
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Identifier",
                "value" => "name"
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => ""
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testtUTF8String () {

        $ast = new \pbql\Parser('name:"çalü"');
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Identifier",
                "value" => "name"
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => "çalü"
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testSimpleStatementStringValueEscaped () {

        $ast = new \pbql\Parser("name:'mr wong\'s'");
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Identifier",
                "value" => "name"
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => "mr wong's"
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    /*public function testSimpleStatementStringValueUTF8 () {

        $ast = new \pbql\Parser("name:'!@#$%^&*()`˜µ≤≥åß∆˚¬…æ«œ∑´®†¥¨ˆøπ“‘’”»");
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Identifier",
                "value" => "name"
            ],
            "right" => (object) [
                "type" => "Value",
                "value" => "mr wong's"
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }*/

    public function testSimpleOrExpression () {

        $ast = new \pbql\Parser('id:1 OR id:2');
        $expectedResult = (object) [
            "type" => "Expression",
            "operator" => "OR",
            "left" => (object) [
                "type"=> "Statement",
                "operator"=> ":",
                "left"=> (object) [
                    "type"=> "Identifier",
                    "value"=> "id"
                ],
                "right"=> (object) [
                    "type"=> "Value",
                    "value"=> 1
                ]
            ],
            "right" => (object) [
                "type"=> "Statement",
                "operator"=> ":",
                "left"=> (object) [
                    "type"=> "Identifier",
                    "value"=> "id"
                ],
                "right"=> (object) [
                    "type"=> "Value",
                    "value"=> 2
                ]
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testSimpleAndExpression () {

        $ast = new \pbql\Parser('id:1 AND id:2');
        $expectedResult = (object) [
            "type" => "Expression",
            "operator" => "AND",
            "left" => (object) [
                "type"=> "Statement",
                "operator"=> ":",
                "left"=> (object) [
                    "type"=> "Identifier",
                    "value"=> "id"
                ],
                "right"=> (object) [
                    "type"=> "Value",
                    "value"=> 1
                ]
            ],
            "right" => (object) [
                "type"=> "Statement",
                "operator"=> ":",
                "left"=> (object) [
                    "type"=> "Identifier",
                    "value"=> "id"
                ],
                "right"=> (object) [
                    "type"=> "Value",
                    "value"=> 2
                ]
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testLargerExpression () {

        $ast = new \pbql\Parser('foo:1 OR bar:"string" OR baz:3');
        $expectedResult = (object) [
            "type" => "Expression",
            "operator" => "OR",
            "left" => (object) [
                "type"=> "Statement",
                "operator"=> ":",
                "left"=> (object) [
                    "type"=> "Identifier",
                    "value"=> "foo"
                ],
                "right"=> (object) [
                    "type"=> "Value",
                    "value"=> 1
                ]
            ],
            "right" => (object) [
                "type" => "Expression",
                "operator" => "OR",
                "left" => (object) [
                    "type"=> "Statement",
                    "operator"=> ":",
                    "left"=> (object) [
                        "type"=> "Identifier",
                        "value"=> "bar"
                    ],
                    "right"=> (object) [
                        "type"=> "Value",
                        "value"=> "string"
                    ]
                ],
                "right" => (object) [
                    "type"=> "Statement",
                    "operator"=> ":",
                    "left"=> (object) [
                        "type"=> "Identifier",
                        "value"=> "baz"
                    ],
                    "right"=> (object) [
                        "type"=> "Value",
                        "value"=> 3
                    ]
                ]
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testGroupSingleStatementShouldReturnStatement () {

        $ast = new \pbql\Parser('(id:1)');
        $expectedResult = (object) [
            "type" => "Group",
            "value" => (object) [
                "type" => "Statement",
                "operator" => ":",
                "left" => (object) [
                    "type" => "Identifier",
                    "value" => "id"
                ],
                "right" => (object) [
                    "type" => "Value",
                    "value" => 1
                ]
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testGroupSingleStatementShouldReturnExpression () {

        $ast = new \pbql\Parser('(id:1 OR id:2)'); // Expression
        $expectedResult = (object) [
            "type" => "Group",
            "value" => (object) [
                "type" => "Expression",
                "operator" => "OR",
                "left" => (object) [
                    "type"=> "Statement",
                    "operator"=> ":",
                    "left"=> (object) [
                        "type"=> "Identifier",
                        "value"=> "id"
                    ],
                    "right"=> (object) [
                        "type"=> "Value",
                        "value"=> 1
                    ]
                ],
                "right" => (object) [
                    "type"=> "Statement",
                    "operator"=> ":",
                    "left"=> (object) [
                        "type"=> "Identifier",
                        "value"=> "id"
                    ],
                    "right"=> (object) [
                        "type"=> "Value",
                        "value"=> 2
                    ]
                ]
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testGroupStatement () {

        $ast = new \pbql\Parser("(foo:'bar' AND id:1) OR fruit:'banana'"); // Expression
        $expectedResult = (object) [
            "type" => "Expression",
            "operator" => "OR",
            "left" => (object) [
                "type"  => "Group",
                "value" => (object) [
                    "type" => "Expression",
                    "operator" => "AND",
                    "left" => (object) [
                        "type" => "Statement",
                        "operator" => ":",
                        "left" => (object) [
                            "type" => "Identifier",
                            "value" => "foo",
                        ],
                        "right" => (object) [
                            "type" => "Value",
                            "value" => "bar",
                        ]
                    ],
                    "right" => (object) [
                        "type" => "Statement",
                        "operator" => ":",
                        "left" => (object) [
                            "type" => "Identifier",
                            "value" => "id",
                        ],
                        "right" => (object) [
                            "type" => "Value",
                            "value" => 1,
                        ]
                    ]
                ]
            ],
            "right" => (object) [
                "type" => "Statement",
                "operator" => ":",
                "left" => (object) [
                    "type" => "Identifier",
                    "value" => "fruit",
                ],
                "right" => (object) [
                    "type" => "Value",
                    "value" => "banana",
                ]
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testParseFunction () {

        $ast = new \pbql\Parser('id:NOW()');
        $expectedResult = (object) [
            "type" => "Statement",
            "operator" => ":",
            "left" => (object) [
                "type" => "Identifier",
                "value" => "id"
            ],
            "right" => (object) [
                "type" => "Function",
                "value" => "NOW()"
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    public function testParseFunctionInGroup () {

        $ast = new \pbql\Parser('(id:NOW())');
        $expectedResult = (object) [
            "type" => "Group",
            "value" => (object) [
                "type" => "Statement",
                "operator" => ":",
                "left" => (object) [
                    "type" => "Identifier",
                    "value" => "id"
                ],
                "right" => (object) [
                    "type" => "Function",
                    "value" => "NOW()"
                ]
            ]
        ];

        $this->assertEquals($ast->node, $expectedResult);

        // json string is a bit easier to debug, but properties could be scrambled
        // $this->assertEquals(json_encode($ast->node), json_encode($expectedResult));
    }

    // function testing must be added
    // 'date:now()'
    // '(date:now())' --> fails
}
