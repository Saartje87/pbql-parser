# pbql

Pluxbox query language.

# Proposal v2.0

> Must be backwards compatible.  
> Syntax must be usable in http /GET request.

~~~php
<?php
// Using 'Model_Broadcast' from radiobox2

// Find
"item.fragment.id>1";

// page-index, max-results, group-by order in pbql string
"name~'a*', page-index:2, max-results:100, order:name:desc";

// Specify relations
"relations:item, item.image, programme, presenter";
// Include all children of model item
"relations:item, item.*";

// Relation ordering
"order:name:asc, order:item.name:desc";
~~~