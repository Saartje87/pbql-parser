<?php
/**
 * Autoload for PHPUnit
 */

spl_autoload_register(function ( $className ) {

    // $path = strtolower($className);
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $className);

    $path = getcwd().'/src/'.$path;

    $path = str_replace('/demo', '', $path);

    if( !is_readable($path.'.php') ) {

        // throw new \Exception($path);
        return false;
    }

    include $path.'.php';
});
